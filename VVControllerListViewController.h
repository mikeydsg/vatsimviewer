//
//  VVControllerListViewController.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/11/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VVControllerListViewController : NSViewController <NSTableViewDelegate>
@property (strong) IBOutlet NSArrayController *controllerArrayController;
@property (strong) IBOutlet NSArrayController *observerArrayController;

@property NSMutableArray * store;
@property NSMutableArray * observers;

@property (weak) IBOutlet NSBox *controllerBox;
@property (weak) IBOutlet NSBox *observerBox;



@property (weak) IBOutlet NSTableView *controllerTableView;
@property (weak) IBOutlet NSTableView *observerTableView;


-(void)updateData;

@end
