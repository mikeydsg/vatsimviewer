//
//  VVControllerListViewController.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/11/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVControllerListViewController.h"
#import "VVATC.h"
#import "VVControllerDetail.h"


@interface VVControllerListViewController ()
@property VVControllerDetail * detail;

@end

@implementation VVControllerListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    return self;
}

-(void) awakeFromNib
{
    _controllerArrayController.content=_store;
    _observerArrayController.content=_observers;
    [_controllerTableView setTarget:self];
    
    [_controllerTableView setDoubleAction:@selector(controllerDoubleClick:)];
    [_observerTableView setTarget:self];
    [_observerTableView setDoubleAction:@selector(observerDoubleClick:)];
    
    
    
}

-(BOOL) tableView:(NSTableView *)tableView shouldSelectTableColumn:(NSTableColumn *)tableColumn
{
    return NO;
    
}

- (void)controllerDoubleClick:(id)object {
    //NSLog(@"Controller Double Clicked!");
    
    unsigned long row = _controllerTableView.selectedRow;
    VVATC * atc = [_controllerArrayController.arrangedObjects objectAtIndex:row];
    
    //NSLog(@"ATC: %@",atc.realname);
    
    _detail=[[VVControllerDetail alloc] initWithWindowNibName:@"VVControllerDetail"];
    _detail.atc=atc;
    
    [_detail showWindow:nil];
    


}
- (void)observerDoubleClick:(id)object {
    //NSLog(@"Controller Double Clicked!");
    
    unsigned long row = _observerTableView.selectedRow;
    VVATC * atc = [_observerArrayController.arrangedObjects objectAtIndex:row];
    
    //NSLog(@"ATC: %@",atc.realname);
    
    _detail=[[VVControllerDetail alloc] initWithWindowNibName:@"VVControllerDetail"];
    _detail.atc=atc;
    
    [_detail showWindow:nil];
    
    
    
}


-(void)updateData
{
    _controllerArrayController.content=_store;
    _observerArrayController.content=_observers;
    
    [_controllerTableView reloadData];
    [_observerTableView reloadData];
    
    NSString * count = [NSString stringWithFormat:@"%lu",_store.count];
    
    NSString * title = [[@"Controllers (" stringByAppendingString:count] stringByAppendingString:@")"];
    
    _controllerBox.title=title;
    
    count = [NSString stringWithFormat:@"%lu",_observers.count];
    title = [[@"Observers (" stringByAppendingString:count] stringByAppendingString:@")"];
    _observerBox.title=title;
    

    
}


@end
