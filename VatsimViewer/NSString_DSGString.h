//
//  NSString_DSGString.h
//  RadarScope
//
//  Created by Mike Evans on 1/15/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//




@interface NSString (DSGString)
+(NSString *)scs:(const char*) inString;

-(BOOL) contains:(NSString * ) string;
-(const char *) cString;
-(NSString * ) compact;
-(unsigned long) find:(NSString *) findStr;

-(unsigned long) numparms: (NSString *) parmchar;

-(NSString * ) parm:(unsigned long) index withSep:(NSString *) sep;
-(NSString *) sparm:(unsigned long) index;
-(NSString *) pparm:(unsigned long) index;
-(NSString *) cparm:(unsigned long) index;
-(NSString *) left: (unsigned long) index;
-(NSString *) right: (unsigned long) index;

-(NSString *) add:(NSString *) inString;


-(NSString *) trim;
-(NSString*) fullTrim;




- (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet;

+(NSString *) dstr:(double) inDouble;


@end

@implementation NSString (DSGString)

+(NSString *)scs:(const char*) inString
{
    return [NSString stringWithCString:inString encoding:NSUTF8StringEncoding];
    
}

+(NSString *) dstr:(double) inDouble
{
    return [NSString stringWithFormat:@"%.0f",inDouble];
}

-(unsigned long) find:(NSString *) findStr
{
    
    return [self rangeOfString:findStr].location;
    
}


-(NSString *) add:(NSString *) inString
{
    return [self stringByAppendingString:inString];
}


-(BOOL) contains:(NSString * ) string
{
    NSRange rng = [self rangeOfString:string options:NSCaseInsensitiveSearch];
    return rng.location != NSNotFound;
}

-(NSString *) compact
{
    return [self stringByReplacingOccurrencesOfString:@"[ ]+"
                                                     withString:@" "
                                                        options:NSRegularExpressionSearch
                                                        range:NSMakeRange(0, self.length)];
}

- (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet {
    NSUInteger location = 0;
    NSUInteger length = [self length];
    unichar charBuffer[length];
    [self getCharacters:charBuffer];
    
    for (length=[self length]; length > 0; length--) {
        if (![characterSet characterIsMember:charBuffer[length - 1]]) {
            break;
        }
    }
    
    return [self substringWithRange:NSMakeRange(location, length - location)];
}



-(NSString *) trim
{
    return [self stringByTrimmingTrailingCharactersInSet:
            [NSCharacterSet whitespaceCharacterSet]];
    
}

-(NSString*) fullTrim
{
    return [self stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceCharacterSet]];
}


-(NSString *) left: (unsigned long) index
{
    if(index>self.length) return @"";
    return [self substringToIndex:index];
}

-(NSString *)right: (unsigned long) index
{
    if(index>self.length) return @"";
    return [self substringFromIndex:[self length]-index];
    
}

-(const char *) cString
{
    return [self cStringUsingEncoding:NSUTF8StringEncoding];
}


-(unsigned long) numparms: (NSString *) parmchar
{
    if(![self contains:parmchar]) return 1;
    return [[self componentsSeparatedByString:parmchar] count];
    
}

-(NSString * ) parm:(unsigned long) index withSep:(NSString *) sep
{
    if([[self componentsSeparatedByString:sep] count] >=index) return [[self componentsSeparatedByString:sep] objectAtIndex:index];
    return @"";
}

-(NSString *) sparm:(unsigned long) index
{
    return [self parm:index withSep:@" "];
}
-(NSString *) pparm:(unsigned long) index
{
    return [self parm:index withSep:@"|"];
}

-(NSString *) cparm:(unsigned long) index
{
    return [self parm:index withSep:@":"];
}

@end
