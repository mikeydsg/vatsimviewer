//
//  RSRadarTools.h
//  MSRC
//
//  Created by Mike Evans on 2/7/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "math.h"






@interface RSRadarTools : NSObject

+(NSString* ) time;


+(NSString*) tempalt:(int) alt;
+(NSString *) FLalt:(int) alt withTransitionAltitude:(int) transalt;
+(NSString *) alt:(int) alt;

+(NSString *) speed:(int) speed;
+(NSString *) transponder:(int) trans;

+(RSGeoPoint) getpointfromGeoPoint:(RSGeoPoint) orig heading:(double) dheading distance:(double) ddist;
+(NSString*)  formatComputerID:(int) inid;  //Format a CID

+(NSString *) formatFrequency:(NSString*) freq;
+(NSString *)formatFrequencyInt:(int) freq;
+(int) frequencyFromString:(NSString*) inString;



+(NSString *) compass:(int) hdg;

+(NSString *) oclock:(int) hdg;

+(double) headingFromPoint1:(RSGeoPoint) pt1 toPoint2:(RSGeoPoint) pt2;
+(double) distancefromPoint1:(RSGeoPoint) pt1 toPoint2:(RSGeoPoint) pt2;

@end
