//
//  RSRadarTools.m
//  MSRC
//
//  Created by Mike Evans on 2/7/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "RSRadarTools.h"



@implementation RSRadarTools

const double torad = (M_PI / 180);

const double tonm=180*60/M_PI;
const double todeg=180/M_PI;


+(NSString*)  formatComputerID:(int) inid  //Format a CID
{
	if( inid < 10) return [[@"00" add:[NSString stringWithFormat:@"%d",inid]] trim];
	if (inid < 100) return [[@"0" add:[NSString stringWithFormat:@"%d",inid]] trim];
	return [[NSString stringWithFormat:@"%d",inid] trim];
}



+(RSGeoPoint) getpointfromGeoPoint:(RSGeoPoint) orig heading:(double) dheading distance:(double) ddist
{
    double lat;
    double lon;
    
	double d=ddist*(M_PI/(180*60));
	double lat1=orig.lat*torad;
	double lon1=-orig.lon;
	lon1=lon1*torad;
	double tc=dheading*torad;
	lat=asin(sin(lat1)*cos(d)+cos(lat1)*sin(d)*cos(tc));
	if (cos(lat)==0) lon=lon1;      // endpoint a pole
	else lon=fmod((lon1-asin(sin(tc)*sin(d)/cos(lat))+M_PI),(2*M_PI))-M_PI;
	lat=lat*todeg;
	lon=lon*todeg;
	lon=-lon;
    RSGeoPoint tem;
    tem.lat=lat;
    tem.lon=lon;
    return tem;
    
}

+(NSString *) formatFrequency:(NSString*) freq
{
    if(freq.length==6)
    {
        return [[[freq left:3] stringByAppendingString:@"."] stringByAppendingString:[freq right:3]];
        
    }
    return nil;
}

+(NSString *)formatFrequencyInt:(int) freq
{
    NSString * tem = [NSString stringWithFormat:@"%d",freq];
    return [RSRadarTools formatFrequency:tem];
}

+(int) frequencyFromString:(NSString*) inString
{
    if(inString.length==6) return atoi([inString cString]);
    if(inString.length==7) return atoi([[[inString left:3] stringByAppendingString: [inString right:3]] cString]);
    return 0;
    
}



+(double) distancefromPoint1:(RSGeoPoint) pt1 toPoint2:(RSGeoPoint) pt2
{
    

    double d;
    double lat1,lat2,lon1,lon2;
    lat1=pt1.lat * torad;
    lat2=pt2.lat * torad;
    lon1=pt1.lon * torad;
    lon2=pt2.lon * torad;
    
    d=2*asin(sqrt(pow((sin((lat1-lat2)/2)),2)+cos(lat1)*cos(lat2)*pow((sin((lon1-lon2)/2)),2)));
    d=d*tonm;
    return d;
}



+(NSString*) tempalt:(int) alt
{
	int newalt = (int) (double)(alt / 100.0f);
    if (newalt < -10)
        return [@"N" add: [[NSString stringWithFormat:@"%d",abs(newalt)] trim] ];
    if (newalt < 0)
        return [@"N" add: [[NSString stringWithFormat:@"%d",abs(newalt)] trim] ];
    if (newalt == 0)
        return @"0";
    if (newalt < 10)
        return [[NSString stringWithFormat:@"%d",newalt] trim];
    if (newalt < 100)
        return [[NSString stringWithFormat:@"%d",newalt] trim];
    return [[NSString stringWithFormat:@"%d",newalt] trim];
}

+(NSString *) FLalt:(int) alt withTransitionAltitude:(int) transalt
{
    if(alt>transalt)
    {
        return [@"FL" add: [RSRadarTools alt:alt]];
    }
    return [NSString stringWithFormat:@"%.0f",((double)(alt/100.0)*100.0)];
}


+(NSString *) alt:(int) alt
{
	int newalt;
    
	if(fmod((double)alt,100.0)<=50) newalt=(int)floor(alt/100.0); else newalt=(int) ceil(alt/100.0);
    
    //int newalt = (int) (alt / 100.0f);
    
	//ceil
    if(alt==-5222)
        return @"VFR";
	if(alt==-5111)
		return @"OTP";
    if (newalt < -10)
        return [@"N" add: [[NSString stringWithFormat:@"%d",abs(newalt)] trim] ];
    if (newalt < 0)
        return [@"N0" add: [[NSString stringWithFormat:@"%d",abs(newalt)] trim] ];
    if (newalt == 0)
        return @"000";
    if (newalt < 10)
        return [@"00" add: [[NSString stringWithFormat:@"%d",newalt] trim] ];
    if (newalt < 100)
        return [@"0" add: [[NSString stringWithFormat:@"%d",newalt] trim] ];
    return [[NSString stringWithFormat:@"%d",newalt] trim];
}

+(NSString *) speed:(int) speed
{
    int newspd = (int) ceil((double)speed / 10);
    if (newspd < 10)
        return [@"0" add: [[NSString stringWithFormat:@"%d",newspd] trim] ];
    return [[NSString stringWithFormat:@"%d",newspd] trim];
}
+(NSString *) transponder:(int) trans
{
    int newalt = trans;
    if (newalt < 10)
        return [@"000" add: [[NSString stringWithFormat:@"%d",newalt] trim] ];
    if (newalt < 100)
        return [@"00" add: [[NSString stringWithFormat:@"%d",newalt] trim] ];
    if (newalt < 1000)
        return [@"0" add: [[NSString stringWithFormat:@"%d",newalt] trim] ];
    return [[NSString stringWithFormat:@"%d",newalt] trim];
}

+(NSString *) compass:(int) hdg
{
    
	if((hdg>337)||(hdg<23)) return @"north";
	if((hdg>22)&&(hdg<68)) return @"northeast";
	if((hdg>67)&&(hdg<113)) return @"east";
	if((hdg>112)&&(hdg<158)) return @"southeast";
    if((hdg>157)&&(hdg<203)) return @"south";
	if((hdg>202)&&(hdg<248)) return @"southwest";
	if((hdg>247)&&(hdg<293)) return @"west";
	if((hdg>292)&&(hdg<338)) return @"northwest";
    
	return @" ";
    
}

+(NSString *) oclock:(int) hdg
{
	if((hdg>=346)||(hdg<=15)) return @"twelve o'clock";
	if((hdg>=16)&&(hdg<=45)) return @"one o'clock";
	if((hdg>=46)&&(hdg<=75)) return @"two o'clock";
	if((hdg>=76)&&(hdg<=105)) return @"three o'clock";
	if((hdg>=106)&&(hdg<=135)) return @"four o'clock";
	if((hdg>=136)&&(hdg<=165)) return @"five o'clock";
	if((hdg>=166)&&(hdg<=195)) return @"six o'clock";
	if((hdg>=196)&&(hdg<=225)) return @"seven o'clock";
	if((hdg>=226)&&(hdg<=255)) return @"eight o'clock";
	if((hdg>=256)&&(hdg<=285)) return @"nine o'clock";
	if((hdg>=286)&&(hdg<=315)) return @"ten o'clock";
	if((hdg>=316)&&(hdg<=345)) return @"eleven o'clock";
    
	return @" ";
}

+(double) headingFromPoint1:(RSGeoPoint) pt1 toPoint2:(RSGeoPoint) pt2
{
    double lat1,lat2,lon1,lon2;
    lat1=pt1.lat*torad;
    lat2=pt2.lat*torad;
    lon1=pt1.lon*torad;
    lon2=pt2.lon*torad;
    
    double tc1=fmod(atan2(sin(lon1-lon2)*cos(lat2),cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(lon1-lon2)), 2*M_PI);
    return tc1;
    
}

+(NSString* ) time
{
    NSDate * now = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDateComponents *parts =  [gregorian components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:now];
    
    
    NSString * outString = [[self speed:(int)parts.hour] add:[self speed:(int)parts.minute]];
    
    

    
    
    
    return outString;
    
    
}


@end
