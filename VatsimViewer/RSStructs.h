//
//  RSSectorStructs.h
//  RadarScope
//
//  Created by Mike Evans on 1/26/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#ifndef RadarScope_RSSectorStructs_h
#define RadarScope_RSSectorStructs_h

typedef struct point
{
    int x;
    int y;
    
} RSPoint;

typedef struct RSGeoPoint
{
    double lat;
    double lon;
    
} RSGeoPoint;



#endif
