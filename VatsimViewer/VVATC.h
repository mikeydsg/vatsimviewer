//
//  VVATC.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/10/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVClient.h"

@interface VVATC : VVClient

@property NSString * frequency;
@property int facilityType;
@property NSString * facilityDescription;
@property NSString * position;
@property NSString * country;
@property NSString * fir;
@property NSString * airport;



@property int visualRange;
@property NSString * atisMessage;


/*
 frequency:4
 facilitytype:18
 visualrange:19
 atis_message:35
 time_last_atis_received:36
 */

@end
