//
//  VVAirport.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/10/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVLocationObject.h"

@interface VVAirport : VVLocationObject

@property NSString * name;
@property NSString * ident;
@property NSString * shortCode;
@property NSString * fir;


@end
