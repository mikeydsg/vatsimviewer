//
//  VVAirport.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/10/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVAirport.h"

@implementation VVAirport
-(NSString *) description
{
    return [_ident stringByAppendingString:[@" - " stringByAppendingString:_name]];    
}

@end
