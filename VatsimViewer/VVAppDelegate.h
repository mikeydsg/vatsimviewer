//
//  VVAppDelegate.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/10/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class VVControllerListViewController;
@class VVPilotListViewController;
@class VVDataDownloader;
@class VVLocalData;




@interface VVAppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSView *customView;


@property VVControllerListViewController * controllers;
@property VVPilotListViewController * pilots;


@property NSViewController * replaceView;

@property NSMutableArray * atcstore;
@property NSMutableArray * pilotstore;
@property NSMutableArray * observerstore;
@property VVDataDownloader * data;
@property VVLocalData * localData;

@property (weak) IBOutlet NSTextField *connectionText;
@property (weak) IBOutlet NSTextField *updatedText;


@property (weak) IBOutlet NSToolbarItem *reloadButton;



@end
