//
//  VVAppDelegate.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/10/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVAppDelegate.h"
#import "VVControllerListViewController.h"
#import "VVPilotListViewController.h"
#import "VVDataDownloader.h"
#import "VVLocalData.h"


@implementation VVAppDelegate

-(void) reloadData
{
    [_reloadButton setEnabled:NO];
    NSMutableArray * tempPilots = [[NSMutableArray alloc] init];
    NSMutableArray * tempControllers = [[NSMutableArray alloc] init];
    NSMutableArray * tempObservers = [[NSMutableArray alloc] init];
    
    
    [_connectionText setStringValue:@"Loading Data..."];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        [_data downloadData:tempControllers withPilotStore:tempPilots observerStore:tempObservers];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _pilots.store=tempPilots;
            _controllers.store=tempControllers;
            _controllers.observers=tempObservers;
            
            [_pilots updateData];
            [_controllers updateData];
            [_reloadButton setEnabled:YES];
            
            [_connectionText setStringValue:_data.connectionInfo];
            
            
            
            //Back on Main Queue:
            
            
        });
        
        
        
        // when that method finishes you can run whatever you need to on the main thread
    });
    
    

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    
    _data = [[VVDataDownloader alloc] init];
    [_data downloadStatus];

    _controllers=[[VVControllerListViewController alloc] initWithNibName:@"VVControllerListViewController" bundle:nil];
    _pilots= [[VVPilotListViewController alloc] initWithNibName:@"VVPilotListViewController" bundle:nil];

    
    _pilotstore = [[NSMutableArray alloc] init];
    _atcstore = [[NSMutableArray alloc] init];
    _observerstore = [[NSMutableArray alloc] init];
    
    

    _pilots.store=_pilotstore;
    _controllers.store=_atcstore;
    _controllers.observers=_observerstore;
    
    
    
    
    

    NSString * datapath = [[NSBundle mainBundle] pathForResource:@"VATViewer" ofType:@"txt"];
    
    
    
    
    _localData = [[VVLocalData alloc] init];
    [_localData parseData:datapath];
    
    _data.data=_localData;
    
    [self reloadData];
    
    
    
    
    
    
    
    
    
    
}



-(void) windowDidResize:(NSNotification *)notification
{
    _replaceView.view.frame=_customView.bounds;
    
}
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}



- (IBAction)selectMap:(id)sender {
}
- (IBAction)selectFlights:(id)sender {
    
    [_replaceView.view removeFromSuperview];
    
    _replaceView=_pilots;
    _replaceView.view.frame=_customView.bounds;
    [_customView addSubview:_replaceView.view];
      [_pilots updateData];
    
    
}
- (IBAction)selectControllers:(id)sender {
    [_replaceView.view removeFromSuperview];
    _replaceView=_controllers;
    _replaceView.view.frame=_customView.bounds;
      [_customView addSubview:_replaceView.view];
    [_controllers updateData];
    
    
}
- (IBAction)selectServers:(id)sender {
}
- (IBAction)selectReload:(id)sender {
    
    [_reloadButton setEnabled:NO];
    
    
    [self reloadData];
    

    
    
    
    
    
    
}

@end
