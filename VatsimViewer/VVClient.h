//
//  VVClient.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/11/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVLocationObject.h"

@interface VVClient : VVLocationObject
@property NSString * callsign;
@property NSString * cid;
@property NSString * realname;

@property NSString * altitude;

@property NSString * server;
@property NSString * protorevision;
@property int rating;
@property NSString * ratingDescription;
@property NSDate * logonTime;
@property NSString * timeOnline;




@end
