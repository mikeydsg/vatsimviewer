//
//  VVControllerDetail.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/16/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "VVATC.h"


@interface VVControllerDetail : NSWindowController
@property (weak) IBOutlet NSTextField *frequencyLabel;
@property (weak) IBOutlet NSTextField *nameLabel;
@property (weak) IBOutlet NSTextField *ratingLabel;
@property (weak) IBOutlet NSTextField *countryLabel;
@property (weak) IBOutlet NSTextField *firLabel;
@property (weak) IBOutlet NSTextField *airportLabel;
@property (weak) IBOutlet NSTextField *timeOnlineLabel;
@property (weak) IBOutlet NSTextField *visualRangeLabel;
@property (weak) IBOutlet NSTextField *serverLabel;
@property (unsafe_unretained) IBOutlet NSTextView *atisBox;
@property (weak) IBOutlet NSTextField *callsignLabel;
@property (weak) IBOutlet NSTextField *positionLabel;
@property (weak) IBOutlet NSButton *vatawareButton;
@property (weak) IBOutlet NSButton *vatsimButton;

@property VVATC * atc;



@end
