//
//  VVControllerDetail.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/16/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVControllerDetail.h"

@interface VVControllerDetail ()

@end

@implementation VVControllerDetail

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    
    [_frequencyLabel setStringValue:_atc.frequency];
    [_nameLabel setStringValue:_atc.realname];
    [_ratingLabel setStringValue:_atc.ratingDescription];
    [_countryLabel setStringValue:_atc.country];
    [_firLabel setStringValue:_atc.fir];
    [_airportLabel setStringValue:_atc.airport];
    [_timeOnlineLabel setStringValue:[_atc.timeOnline fullTrim]];
    [_visualRangeLabel setStringValue:[NSString stringWithFormat:@"%d",_atc.visualRange]];
    [_serverLabel setStringValue:_atc.server];
    [_callsignLabel setStringValue:_atc.callsign];
    [_positionLabel setStringValue:_atc.position];
    [_atisBox setString:_atc.atisMessage];
    NSString * vataware = [NSString stringWithFormat:@"VATAware Stats for CID %@",_atc.cid];
    NSString * vatsim = [NSString stringWithFormat:@"VATSIM Stats for CID %@",_atc.cid];
    [_vatawareButton setTitle:vataware];
    [_vatsimButton setTitle:vatsim];
    
    
    
    
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}
- (IBAction)closeClicked:(id)sender {
    [[self window] orderOut:nil];
    
}
- (IBAction)vatawareClicked:(id)sender
{
    NSString * message = [NSString stringWithFormat:@"http://www.vataware.com/pilot.cfm?cid=%@",_atc.cid];
    
    
    
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:message]];
    
}
- (IBAction)vatsimClicked:(id)sender {
    NSString * message = [NSString stringWithFormat:@"http://stats.vatsim.net/search_id.php?id=%@",_atc.cid];
    
    
    
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:message]];
}

@end
