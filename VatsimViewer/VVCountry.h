//
//  VVCountry.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/15/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VVCountry : NSObject

@property NSString * name;
@property NSString * code;
@property NSString * centerName;


@end
