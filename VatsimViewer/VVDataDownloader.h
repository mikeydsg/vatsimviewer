//
//  VVDataDownloader.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/11/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVLocalData.h"


@interface VVDataDownloader : NSObject

-(void) parseClient:(NSString *)line atcStore:(NSMutableArray * )atcs pilotStore:(NSMutableArray*)pilots observerStore:(NSMutableArray *)observers;
-(void) downloadData:(NSMutableArray *)atcs withPilotStore:(NSMutableArray * )pilots observerStore:(NSMutableArray*)observers;

-(void) downloadStatus;

@property NSMutableArray * dataServers;
@property (weak) VVLocalData * data;
@property NSString * connectionInfo;
@property NSString * lastUpdatedInfo;




@end
