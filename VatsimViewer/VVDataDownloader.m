//
//  VVDataDownloader.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/11/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVDataDownloader.h"
#import "VVClient.h"
#import "VVATC.h"
#import "VVPlane.h"
#import "VVLocalData.h"
#import "VVAirport.h"
#import "VVFIR.h"
#import "VVCountry.h"
#import "RSRadarTools.h"

int numatc;
int numatis;



typedef enum dataFileSections
{
    sectionGeneral = 0,
    sectionClients,
    sectionServers,
    sectionVoiceServers,
    sectionPrefile,
    sectionUnknown
 
} dataFileSections;


@implementation VVDataDownloader
-(NSString *) facility:(int)inFacility forCallsign:(NSString *) callsign
{
    switch (inFacility) {
        case 0:
            return @"OBS";
            
            break;
        case 1:
            return @"FSS";
            break;
        case 2:
            return @"DEL";
            break;
        case 3:
            return @"GND";
            break;
        case 4:
            if(callsign.length>4 && [[callsign right:4] isEqualToString:@"ATIS"]) return @"ATIS";
            return @"TWR";
            break;
        case 5:
            if(callsign.length>3 && [[callsign right:3] isEqualToString:@"DEP"]) return @"DEP";
            
            return @"APP";
            break;
        case 6:
            return @"CTR";
            break;
            
        default:
            return @"UNK";
            break;
    }
}

-(NSString *) positionName:(NSString *) callsign forFIR:(VVFIR*) fir
{
    if(callsign.length<4) return @"";
    
    NSString * suffix = [callsign right:3];
    
    NSArray * suffixes=@[@"CTR",@"APP",@"DEP",@"TWR",@"GND",@"DEL",@"FSS"];
    
    if([suffixes indexOfObject:suffix] != NSNotFound)
    {
        if([suffix isEqualToString:@"CTR"]) return [_data centerNameForFIR:fir.code];
        
        if([suffix isEqualToString:@"APP"]) return @"Approach";
        if([suffix isEqualToString:@"DEP"]) return @"Departure";
        if([suffix isEqualToString:@"TWR"]) return @"Tower";
        if([suffix isEqualToString:@"GND"]) return @"Ground";
        if([suffix isEqualToString:@"DEL"]) return @"Delivery";
        if([suffix isEqualToString:@"FSS"]) return @"Flight Service Station";
        
        
        
    }
    
    suffix=[callsign right:4];
    if([suffix isEqualToString:@"ATIS"]) return @"ATIS";

    
    
    return @"";
}

-(NSString*) flightType:(NSString*)inType
{
    if([inType isEqualToString:@"I"]) return @"IFR";
    if([inType isEqualToString:@"V"]) return @"VFR";
    if([inType isEqualToString:@"D"]) return @"DVFR";
    if([inType isEqualToString:@"S"]) return @"SVFR";
    return @"";
    
}

-(NSString*) rating:(int)inRating
{
    switch(inRating)
    {
        case 0:
            return @"UNK";
            break;
        case 1:
            return @"OBS";
            break;
        case 2:
            return @"S1";
            break;
        case 3:
            return @"S2";
            break;
        case 4:
            return @"S3";
            break;
        case 5:
            return @"C1";
            break;
        case 6:
            return @"C2";
            break;
        case 7:
            return @"C3";
            break;
        case 8:
            return @"I1";
            break;
        case 9:
            return @"I2";
            break;
        case 10:
            return @"I3";
            break;
        case 11:
            return @"SUP";
            break;
        case 12:
            return @"ADM";
            break;
            
    };
    
    return @"";
}


-(BOOL) isATIS:(NSString*) callsign
{
    if(callsign.length<4) return NO;
    
    NSString * suffix = [callsign right:4];
    
    if([suffix isEqualToString:@"ATIS"]) return YES;
    return NO;
    

}

-(BOOL) isATC:(NSString *) callsign
{
    if(callsign.length<4) return NO;
    
    NSString * suffix = [callsign right:3];
    
    NSArray * suffixes=@[@"CTR",@"APP",@"DEP",@"TWR",@"GND",@"DEL",@"FSS"];
    
    if([suffixes indexOfObject:suffix] != NSNotFound) return YES;
    
    suffix=[callsign right:4];
    if([suffix isEqualToString:@"ATIS"]) return YES;
    
    

    
    return NO;
}


-(void) parseClient:(NSString *)line atcStore:(NSMutableArray * )atcs pilotStore:(NSMutableArray*)pilots observerStore:(NSMutableArray *)observers
{
    NSString * clientType=[line cparm:3];
    
    VVClient * client;
    
    /*
  
     QNH_iHg:39
     QNH_Mb:40
     */
    NSString * callsign=[line cparm:0];
    NSString * prefix = @"";
    VVPlane * plane;
    VVATC * atc;
    
    
    if([clientType isEqualToString:@"PILOT"])
    {
        plane = [[VVPlane alloc] init];
        client=plane;
    }
    else if([clientType isEqualToString:@"ATC"])
    {
        atc = [[VVATC alloc] init];
        client=atc;
    }
    
    if(client==nil) return;
    
    
    
    client.callsign=callsign;
    client.cid=[line cparm:1];
    client.realname=[line cparm:2];
    client.rating=[[line cparm:16] intValue];
    client.ratingDescription=[self rating:client.rating];
    
    client.altitude=[line cparm:7];
    
    RSGeoPoint loc;
    loc.lat=[[line cparm:5] doubleValue];
    loc.lon=[[line cparm:6] doubleValue];
    client.loc=loc;
    
    client.protorevision=[line cparm:15];
    client.server=[line cparm:14];
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    
    
    client.logonTime=[dateFormat dateFromString:[line cparm:37]];
    
    
    NSDate * now = [[NSDate alloc] init];
    
    NSCalendar *calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:client.logonTime toDate:now options:0];
    NSString * minuteString=@"0";
    if(dateComponents.minute<10) minuteString = [minuteString stringByAppendingString:[NSString stringWithFormat:@"%ld",dateComponents.minute]];
    else minuteString=[NSString stringWithFormat:@"%ld",dateComponents.minute];
    client.timeOnline=[NSString stringWithFormat:@"%2ld:%@",dateComponents.hour,minuteString];
    
    

    
    
        
    

    
    
    
    if([clientType isEqualToString:@"PILOT"])
    {
        
        plane.aircraftType=[line cparm:9];
        plane.groundspeed=[line cparm:8];
        plane.trueAirspeed=[line cparm:10];
        plane.departureAirport=[line cparm:11];
        
        
        
        plane.plannedAltitude=[line cparm:12];
        plane.destinationAirport=[line cparm:13];
        plane.transponder=[line cparm:17];
        plane.revision=[line cparm:20];
        plane.flightType=[self flightType:[line cparm:21]];
        plane.plannedDepartureTime=[line cparm:22];
        plane.actualDepartureTime=[line cparm:23];
        plane.hoursEnroute=[line cparm:24];
        plane.minutesEnroute=[line cparm:25];
        plane.hoursFuel=[line cparm:26];
        plane.minutesFuel=[line cparm:27];
        plane.alternateAirport=[line cparm:28];
        plane.remarks=[line cparm:29];
        plane.route=[line cparm:30];
        plane.heading=[[line cparm:38] intValue];
        plane.flightStatus=@"En Route";
        plane.airspaceIn=@"";
        plane.distanceFlown=@"Unknown";
        plane.distanceToGo=@"Unknown";
        plane.eta=@"";
        
        plane.etaColor =[NSColor blackColor];

        
        VVAirport * destapt = [_data airport:plane.destinationAirport];

        if(destapt)
        {
            plane.destinationAirportDescription=[plane.destinationAirport stringByAppendingString:[@" - " stringByAppendingString:destapt.name]];
            plane.destinationAirportLoc=destapt.loc;
            
            double dist = [RSRadarTools distancefromPoint1:plane.destinationAirportLoc toPoint2:plane.loc];
            plane.distanceToGo=[NSString stringWithFormat:@"%.1f NM",dist ];
            
            int gs = atof([plane.groundspeed cString]);
            if(gs==0) plane.eta=@"Unknown";
            else
            {
                //Calculate ETA
                double eta = dist/gs;
                int hour = (int)eta;
                int min = (eta-hour)*60;
                if(min<10) plane.eta=[NSString stringWithFormat:@"%d:0%d",hour,min];
                
                else plane.eta=[NSString stringWithFormat:@"%d:%d",hour,min];
                
                
            }
            
            if((dist<2) && (gs<40))
            {
                plane.flightStatus=@"Landed";
                plane.eta=@"Landed";
                plane.etaColor =[NSColor magentaColor];
                
            }
            
            if((dist<2)&&(gs==0))
            {
                plane.flightStatus=@"Arrived";
                plane.eta=@"Arrived";
                plane.etaColor =[NSColor redColor];
            }
            

            
            
            
        }
        else
        {
            plane.destinationAirportDescription=plane.destinationAirport;
            
            
        }
        
        
        
        
        VVAirport * depapt = [_data airport:plane.departureAirport];
        if(depapt)
        {
            plane.departureAirportDescription=[plane.departureAirport stringByAppendingString:[@" - " stringByAppendingString:depapt.name]];
            plane.departureAirportLoc=depapt.loc;
            
            double dist = [RSRadarTools distancefromPoint1:plane.departureAirportLoc toPoint2:plane.loc];
            
            plane.distanceFlown=[NSString stringWithFormat:@"%.1f NM",dist ];
            int gs = atof([plane.groundspeed cString]);

            
            if((dist<2) && (gs<40))
            {
                //Departing
                plane.flightStatus=@"Taxiing";
                plane.eta=@"Taxiing";
                plane.etaColor =[NSColor colorWithRed:0 green:.75 blue:.75 alpha:1];

                
            }
            if((dist<2) && (gs==0))
            {
                //Departing
                plane.flightStatus=@"Departing";
                plane.eta=@"Departing";
                plane.etaColor =[NSColor colorWithRed:0 green:.5 blue:0 alpha:1];
                
            }

            
            

            
        }
        else
        {
            plane.departureAirportDescription=plane.departureAirport;
        }
        
        
        
        
        if([plane.departureAirport isEqualToString:@""])
        {
            //Find Closest Airport
            VVAirport * temApt = [_data closestAirportForLocation:plane.loc];
            plane.departureAirport=temApt.ident;
            plane.departureAirportDescription=[plane.departureAirport stringByAppendingString:[@" - " stringByAppendingString:temApt.name]];
            plane.departureAirportLoc=temApt.loc;
            plane.flightStatus=@"On Ground";
            plane.eta=@"On Ground";
            plane.etaColor =[NSColor colorWithRed:0 green:.5 blue:0 alpha:1];

            
            
            
        }
        
        
        
        //NSLog(@"Add Pilot");
        
        [pilots addObject:plane];
        
        
        
        
        
        
    }
    else if([clientType isEqualToString:@"ATC"])
    {
        /*
         
         time_last_atis_received:36
         */
        unsigned long prefixpos =[callsign find:@"_"];
        if(prefixpos!=NSNotFound) prefix = [callsign substringToIndex:prefixpos];
        
        

        

        atc.frequency=[line cparm:4];
        atc.facilityType=[[line cparm:18] intValue];
        atc.facilityDescription=[self facility:atc.facilityType forCallsign:callsign];
        atc.visualRange=[[line cparm:19] intValue];
        
        
        NSString * atisTemp = [line cparm:35];
        NSString * atisBuild = @"";
        
        NSArray * atisArray = [atisTemp componentsSeparatedByString:@"^§"];
        
        for (NSString * atisline in atisArray)
        {
            if([[atisline left:1] isEqualToString:@"$"])
            {
                
            }
            else
            {
                atisBuild=[[atisBuild add:atisline] add:@"\n"];
                
            }
        }
        
        
        
        atc.atisMessage=atisBuild;
        atc.position=nil;
        
        
        
        
        if([self isATC:callsign])
        {
           [atcs addObject:atc];
            //NSLog(@"Add ATC");
            if([self isATIS:callsign]) numatis++;
            else numatc++;
            
            
            if([_data airport:prefix])
            {
                //Airport Exists that *MATCHES* the prefix
                VVAirport * airport = [_data airport:prefix];
                atc.position=[airport.name add:[@" " add:[self positionName:callsign forFIR:nil]]];
                atc.airport=airport.name;
                NSArray * posFIR = [_data FIRForPrefix:airport.fir];
                if(posFIR.count>0)
                {
                    VVFIR * fir = [posFIR objectAtIndex:0];
                    atc.fir=fir.name;
                    atc.country=[_data countryForPrefix:fir.code].name;
                }
                

                
                
                
                
            }
            else
            {
                if([_data UIRForPrefix:prefix])
                {
                    VVUIR * uir = [_data UIRForPrefix:prefix];
                    atc.position=uir.name;
                    atc.fir=@"N/A";
                    atc.airport=@"N/A";
                    atc.country=@"N/A";
                    
                    
                }
                else
                {
                    
                    
                    
                    
                    
                    NSArray * posFIR = [_data FIRForPrefix:prefix];
                    if(posFIR.count==1)
                    {
                        VVFIR * fir = [posFIR objectAtIndex:0];
                        
                        
                        atc.position=[fir.name add:[@" " add:[self positionName:callsign forFIR:fir]]];
                        atc.airport=@"N/A";
                        atc.fir=fir.name;
                        atc.country=[_data countryForPrefix:fir.code].name;
                        
                        
                        
                    }
                }
  
                if(atc.position==nil)
                {
                    NSArray * posAirports = [_data airportForPrefix:prefix];
                    if(posAirports.count>0)
                    {
                        VVAirport * airport = [posAirports objectAtIndex:0];
                        atc.position=[airport.name add:[@" " add:[self positionName:callsign forFIR:nil]]];
                        atc.airport=airport.name;
                        NSArray * posFIR = [_data FIRForPrefix:airport.fir];
                        if(posFIR.count>0)
                        {
                            VVFIR * fir = [posFIR objectAtIndex:0];
                            atc.fir=fir.name;
                            atc.country=[_data countryForPrefix:fir.code].name;
                        }

                        
                        
                        
                    }
                }
                
                
            }//Airport
            
            if(atc.position==nil)
            {
                atc.position=[@"Unknown " add:[self positionName:callsign forFIR:nil]];
                atc.country=@"N/A";
                atc.fir=@"N/A";
                atc.airport=@"N/A";
                
                
            }
            
        }//IS ATC
        else
        {
            [observers addObject:atc];
            atc.country=@"N/A";
            atc.fir=@"N/A";
            atc.airport=@"N/A";
            atc.position=@"Observer";
            //NSLog(@"Add Observer");
            
        }
        
        
        
    }
    
    
}


-(dataFileSections) determineSection: (NSString*) inLine
{
    //NSLog(@"*%@*",inLine);
    if([inLine isEqualToString:@"!GENERAL:"]) return sectionGeneral;
    if([inLine isEqualToString:@"!VOICE SERVERS:"]) return sectionVoiceServers;
    if([inLine isEqualToString:@"!CLIENTS:"]) return sectionClients;
    if([inLine isEqualToString:@"!SERVERS:"]) return sectionServers;
    if([inLine isEqualToString:@"!PREFILE:"]) return sectionPrefile;
    return sectionUnknown;
}

-(void) downloadData:(NSMutableArray *)atcs withPilotStore:(NSMutableArray * )pilots observerStore:(NSMutableArray*)observers
{
    numatc=0;
    numatis=0;
    
    if(_dataServers.count==0) return;
    
    int r = arc4random() % (_dataServers.count-1);
    
    NSURL * downloadURL = [NSURL URLWithString:_dataServers[r]];
    NSError * error;
    
    NSString * dataFeed = [NSString stringWithContentsOfURL:downloadURL encoding:NSASCIIStringEncoding error:&error];
    
    if(error)
    {
        NSLog(@"ERROR: %@",error.description);
        
    }
    
    NSArray *lineArray = [dataFeed componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
   // NSLog(@"Parsed %lu lines.",lineArray.count);
    
    dataFileSections section = sectionUnknown;
    
    
    for (__strong NSString * loopline in lineArray) {
        NSString * line = [loopline trim];
        if(line.length>0)
        {
            
            
            
            if(![[line left:1] isEqualToString:@";"])
            {
                if([[line left:1] isEqualToString:@"!"])
                {
                    //Line is a section header
                    section=[self determineSection:line];
                }
                else
                {
                    //Data Line
                    switch(section)
                    {
                        case sectionClients:
                            [self parseClient:line atcStore:atcs pilotStore:pilots observerStore:observers];
                            
                            break;
                            
                        default:
                            break;
                            
                            
                    };
                }
            }
        }
    }
    //NSLog(@"Download DONE!");
    unsigned long numpilot=pilots.count;
    unsigned long numobs=observers.count;
    unsigned long total=numpilot+numobs+numatis+numatc;
    
    _connectionInfo=[NSString stringWithFormat:@"%lu total connections (%lu pilots, %d controllers, %d ATISes, %lu observers)",total,numpilot,numatc,numatis,numobs];
    
    
    
}

- (id)init
{
    self = [super init];
    if (self) {
        _dataServers = [[NSMutableArray alloc] init];
        
    }
    return self;
}
-(void) downloadStatus
{
    NSURL * statusURL = [NSURL URLWithString:@"http://status.vatsim.net"];
    
    NSString * statusFile = [NSString stringWithContentsOfURL:statusURL encoding:NSASCIIStringEncoding error:nil];
    
    
    
    NSArray *lineArray = [statusFile componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    //NSLog(@"Parsed %lu lines.",lineArray.count);
    
    for (NSString * line in lineArray) {
        if([[[line left:5] uppercaseString] isEqualToString:@"URL0="])
        {
            //NSLog(@"URL:*%@*",[line right:line.length-5]);
            [_dataServers addObject:[line right:line.length-5]];
        }
    }
}

@end
