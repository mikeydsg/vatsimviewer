//
//  VVFIR.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/15/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VVFIR : NSObject
@property NSString * code;
@property NSString * name;
@property NSString * prefix;
@property NSString * parent;


@end
