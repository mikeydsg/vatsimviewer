//
//  VVFIR.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/15/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVFIR.h"

@implementation VVFIR
-(NSString *) description
{
    return [_code stringByAppendingString:[@" - " stringByAppendingString:_name]];
}

@end
