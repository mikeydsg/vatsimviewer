//
//  VVLocalData.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/15/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVCountry.h"
#import "VVAirport.h"
#import "VVUIR.h"

@interface VVLocalData : NSObject


@property NSMutableArray * countries;
@property NSMutableArray * airports;
@property NSMutableArray * firs;
@property NSMutableArray * uirs;

//@property NSMutableDictionary * airportPrefix;




-(bool) parseData: (NSString *) path;

-(NSArray *) airportForPrefix:(NSString * )prefix;
-(NSArray *) FIRForPrefix:(NSString * )prefix;
-(NSString*) centerNameForFIR:(NSString *) inString;
-(VVCountry *) countryForPrefix:(NSString *) prefix;
-(VVAirport *)airport:(NSString*) ident;
-(VVUIR*) UIRForPrefix:(NSString *)prefix;
-(VVAirport*) closestAirportForLocation:(RSGeoPoint) loc;






@end
