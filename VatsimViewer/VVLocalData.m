//
//  VVLocalData.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/15/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVLocalData.h"
#import "VVFIR.h"
#import "VVAirport.h"
#import "VVCountry.h"
#import "VVUIR.h"
#import "RSRadarTools.h"


typedef enum datasections
{
    section_country,
    section_airport,
    section_fir,
    section_uir,
    section_idl,
    section_unknown
    
} datasections;


@implementation VVLocalData

datasections cursec;

- (id)init
{
    self = [super init];
    if (self) {
        _countries = [[NSMutableArray alloc] init];
        _firs = [[NSMutableArray alloc] init];
        _uirs = [[NSMutableArray alloc] init];
        _airports = [[NSMutableArray alloc] init];
        //_airportPrefix = [[NSMutableDictionary alloc] init];
        
        
    }
    return self;
}

-(BOOL) checksection:(NSString *) inString
{
    
    if([inString isEqualToString:@"[Countries]"])
    {
        cursec=section_country;
        return TRUE;
    }
    if([inString isEqualToString:@"[Airports]"])
    {
        cursec=section_airport;
        return TRUE;
        
    }
    if([inString isEqualToString:@"[FIRs]"])
    {
        cursec=section_fir;
        return TRUE;
        
    }
    if([inString isEqualToString:@"[UIRs]"])
    {
        cursec=section_uir;
        return TRUE;
        
    }
    if([inString isEqualToString:@"[IDL]"])
    {
        cursec=section_idl;
        return TRUE;
        
    }

    
    
    
    return FALSE;
    
}

-(void) parseFIR:(NSString*)inLine
{
    //NSLog(@"%@",inLine);
    if([inLine numparms:@"|"]==4)
    {
        VVFIR * fir= [[VVFIR alloc] init];
        fir.code=[inLine pparm:0];
        fir.name=[inLine pparm:1];
        fir.prefix=[inLine pparm:2];
        fir.parent=[inLine pparm:3];
        
        [_firs addObject:fir];
        
        
        
    }
    else
    {
        NSLog(@"FIR Parse Error: %@",inLine);
    }
    
}

-(void) parseUIR:(NSString*)inLine
{
    //NSLog(@"%@",inLine);
    if([inLine numparms:@"|"]==3)
    {
        VVUIR * uir= [[VVUIR alloc] init];
        uir.code=[inLine pparm:0];
        uir.name=[inLine pparm:1];
        NSString * tem = [inLine pparm:2];
        uir.firs=[[tem componentsSeparatedByString:@","] mutableCopy];
        
        
        [_uirs addObject:uir];
        
        
        
    }
    else
    {
        NSLog(@"FIR Parse Error: %@",inLine);
    }
    
}




-(void) parseAirport:(NSString *)inLine
{
    //NSLog(@"%@",inLine);
    if([inLine numparms:@"|"]==7)
    {
        VVAirport * airport = [[VVAirport alloc] init];
        airport.ident=[inLine pparm:0];
        airport.name=[inLine pparm:1];
        RSGeoPoint loc;
        loc.lat=[[inLine pparm:2] doubleValue];
        loc.lon=[[inLine pparm:3] doubleValue];
        airport.loc=loc;
        airport.shortCode=[inLine pparm:4];
        airport.fir=[inLine pparm:5];
        
        
        [_airports addObject:airport];
        
        
    }
    else
    {
        NSLog(@"Airport Parse Error: %@",inLine);
    }
    
    
    
}

-(void) parseCountry:(NSString*)inLine
{
    //NSLog(@"%@",inLine);
    if([inLine numparms:@"|"]==3)
    {
        VVCountry * country = [[VVCountry alloc] init];
        country.name=[inLine pparm:0];
        country.code=[inLine pparm:1];
        country.centerName=[inLine pparm:2];
        if(country.centerName.length==0) country.centerName=@"Center";
        
        [_countries addObject:country];
        
        
        
    }
    else
    {
        NSLog(@"Country Parse Error: %@",inLine);
        
    }
    
}

-(void) doSection:(NSString*) inLine
{
    switch (cursec)
    {
        case section_unknown:
            return;
            break;
        case section_country:
            [self parseCountry:inLine];
            break;
        case section_airport:
            [self parseAirport:inLine];
            break;
        case section_uir:
            [self parseUIR:inLine];
            break;
        case section_fir:
            [self parseFIR:inLine];
            
            break;
        case section_idl:
            break;
    }
    
}


-(NSString *) readLineAsNSString: (FILE *) file
{
    char buffer[4096];
    
    // tune this capacity to your liking -- larger buffer sizes will be faster, but
    // use more memory
    NSMutableString *result = [NSMutableString stringWithCapacity:256];
    
    // Read up to 4095 non-newline characters, then read and discard the newline
    int charsRead;
    do
    {
        if(fscanf(file, "%4095[^\n]%n%*c", buffer, &charsRead) == 1)
            [result appendFormat:@"%s", buffer];
        else
            break;
    } while(charsRead == 4095);
    
    return result;
}

-(VVUIR*) UIRForPrefix:(NSString *)prefix
{
    for(VVUIR * uir in _uirs)
    {
        if([uir.code isEqualToString:prefix])
            return uir;
        
    }
    
    return nil;
}

-(NSArray *) FIRForPrefix:(NSString * )prefix
{
    NSMutableArray * tempArray =[[NSMutableArray alloc] init];
    
    
    for(VVFIR * fir in _firs)
    {
        if([fir.code isEqualToString:prefix])
        {
            [tempArray addObject:fir];
            return tempArray;
            
        }
        
        
        if([fir.prefix isEqualToString:prefix])
        {
            [tempArray addObject:fir];
            
        }
        
        
    }
    
    
    return tempArray;
    
}

-(VVCountry *) countryForPrefix:(NSString *) inprefix
{
    if(inprefix==nil) return nil;
    
    NSString * prefix=inprefix;
    
    if(inprefix.length>2) prefix=[inprefix left:2];
    
    for(VVCountry * country in _countries)
    {
        if([country.code isEqualToString:prefix]) return country;
    }
    return nil;
    
}

-(NSString*) centerNameForFIR:(NSString *) inString
{
    NSString * countryCode=[inString left:2];
    
    VVCountry * country = [self countryForPrefix:countryCode];
    
    if(country.centerName.length>0) return country.centerName;
    return @"Center";
    
    
    
    
}

-(VVAirport *)airport:(NSString*) ident
{
    for(VVAirport * airport in _airports)
    {
        if([airport.ident isEqualToString:ident])
        {
            return airport;
        }
        
        
    }
    return nil;
    

}

-(VVAirport*) closestAirportForLocation:(RSGeoPoint) loc
{
    VVAirport * closest=nil;
    double dist=999999;
    double temdist;
    
    
    for(VVAirport * airport in _airports)
    {
        temdist= [RSRadarTools distancefromPoint1:airport.loc toPoint2:loc];
        
        if(temdist<dist)
        {
            dist=temdist;
            closest=airport;
            
        }
        
        
    }
    return closest;
    
    
}

-(NSArray *) airportForPrefix:(NSString * )prefix
{
    NSMutableArray * tempArray =[[NSMutableArray alloc] init];
    
    
    for(VVAirport * airport in _airports)
    {
        if([airport.ident isEqualToString:prefix])
        {
            [tempArray addObject:airport];
            return tempArray;
        }
        
        
        if([airport.shortCode isEqualToString:prefix])
        {
            [tempArray addObject:airport];
            
        }
        
        
    }
    
    
    return tempArray;
    
}



-(bool) parseData: (NSString *) path
{
    cursec=section_unknown;
    

    unsigned int numlines=0;
    
    
    FILE *file = fopen([path cStringUsingEncoding:NSUTF8StringEncoding], "r");
    // check for NULL
    while(!feof(file))
    {
        NSString *line = [self readLineAsNSString:file];
        //NSLog(@"LINE: %@",line);
        // do stuff with line; line is autoreleased, so you should NOT release it (unless you also retain it beforehand)
        @autoreleasepool {
            
            
            //line = [line uppercaseString] ;
            line = [line stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
            line = [line componentsSeparatedByString:@";"][0];
            line = [line trim];
            
            if([line length] >0)
            {
                if([line characterAtIndex:0]=='[')
                {
                    
                    if(![self checksection:line])
                    {
                        [self doSection:line];
                        numlines++;
                    }
                }
                else
                {
                    
                    [self doSection:line];
                    numlines++;
                    
                }
                
            }
        }
        
        
        
        
        
    }
    fclose(file);
    
    
    //NSLog(@"Processed %u lines!",numlines);
    
    
    return true;
    
    
}



@end
