//
//  VVLocationObject.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/10/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface VVLocationObject : NSObject
@property RSGeoPoint loc;


@end
