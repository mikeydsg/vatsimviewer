//
//  VVPilotDetail.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/16/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "VVPlane.h"

@interface VVPilotDetail : NSWindowController
@property (weak) IBOutlet NSTextField *callsignLabel;
@property (weak) IBOutlet NSTextField *summaryLabel;
@property (weak) IBOutlet NSTextField *flightStatusLabel;
@property (weak) IBOutlet NSTextField *nameLabel;
@property (weak) IBOutlet NSTextField *airspaceLabel;
@property (weak) IBOutlet NSTextField *distanceFlownLabel;
@property (weak) IBOutlet NSTextField *distanceTogoLabel;
@property (weak) IBOutlet NSTextField *altitudeLabel;
@property (weak) IBOutlet NSTextField *timeOnlineLabel;
@property (weak) IBOutlet NSTextField *groundspeedLabel;
@property (weak) IBOutlet NSTextField *serverLabel;
@property (weak) IBOutlet NSTextField *headingLabel;
@property (weak) IBOutlet NSTextField *etaLabel;
@property (weak) IBOutlet NSTextField *flightRulesLabel;
@property (weak) IBOutlet NSTextField *trueAirspeedLabel;
@property (weak) IBOutlet NSTextField *aircraftTypeLabel;
@property (weak) IBOutlet NSTextField *cruiseAltitudeLabel;
@property (weak) IBOutlet NSTextField *departureAirportLabel;
@property (weak) IBOutlet NSTextField *destinationAirportLabel;

@property (unsafe_unretained) IBOutlet NSTextView *routeBox;
@property (unsafe_unretained) IBOutlet NSTextView *remarksBox;
@property (weak) IBOutlet NSButton *vatawareCallsignButton;

@property (weak) IBOutlet NSButton *vatawareCIDButton;
@property (weak) IBOutlet NSButton *vatsimButton;


@property VVPlane * plane;


@end
