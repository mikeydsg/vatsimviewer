//
//  VVPilotDetail.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/16/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVPilotDetail.h"

@interface VVPilotDetail ()

@end

@implementation VVPilotDetail

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

-(void) updateData
{
    [_callsignLabel setStringValue:_plane.callsign];
    [_summaryLabel setStringValue:[[_plane.departureAirport add: @" > "] add: _plane.destinationAirport]];
    [_flightStatusLabel setStringValue:_plane.flightStatus];
    [_nameLabel setStringValue:[NSString stringWithFormat:@"%@ (%@)",_plane.realname,_plane.cid]];
    [_airspaceLabel setStringValue:_plane.airspaceIn];
    [_distanceFlownLabel setStringValue:_plane.distanceFlown];
    [_distanceTogoLabel setStringValue:_plane.distanceToGo];
    [_altitudeLabel setStringValue:_plane.altitude];
    [_timeOnlineLabel setStringValue:[_plane.timeOnline fullTrim]];
    [_groundspeedLabel setStringValue:_plane.groundspeed];
    [_serverLabel setStringValue:_plane.server];
    [_headingLabel setStringValue:[NSString stringWithFormat:@"%d",_plane.heading]];
    [_etaLabel setStringValue:_plane.eta];
    //TODO: may need fixing:
    
    [_flightRulesLabel setStringValue:_plane.flightType];
    [_trueAirspeedLabel setStringValue:_plane.trueAirspeed];
    [_aircraftTypeLabel setStringValue:_plane.aircraftType];
    [_cruiseAltitudeLabel setStringValue:_plane.plannedAltitude];
    [_departureAirportLabel setStringValue:_plane.departureAirportDescription];
    [_destinationAirportLabel setStringValue:_plane.destinationAirportDescription];
    
    [_routeBox setString:_plane.route];
    [_remarksBox setString:_plane.remarks];
    
    NSString * vatawareCall=[NSString stringWithFormat:@"VATAware Stats for %@",_plane.callsign];
    NSString * vataware = [NSString stringWithFormat:@"VATAware Stats for CID %@",_plane.cid];
    NSString * vatsim = [NSString stringWithFormat:@"VATSIM Stats for CID %@",_plane.cid];
    [_vatawareCIDButton setTitle:vataware];
    [_vatawareCallsignButton setTitle:vatawareCall];
    
    
    [_vatsimButton setTitle:vatsim];

    
}


- (void)windowDidLoad
{
    [super windowDidLoad];
    
    [self updateData];
    
    
    
    
}


-(void) showWindow:(id)sender
{
    
    [self updateData];
    [super showWindow:sender];
    
    
    
}

- (IBAction)closeClicked:(id)sender {
    [[self window] orderOut:nil];
    
}
- (IBAction)vatawareCallClicked:(id)sender
{
    NSString * message = [NSString stringWithFormat:@"http://www.vataware.com/callsign.cfm?callsign=%@",_plane.callsign];
    
    
    
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:message]];
    
}



- (IBAction)vatawareClicked:(id)sender
{
    NSString * message = [NSString stringWithFormat:@"http://www.vataware.com/pilot.cfm?cid=%@",_plane.cid];
    
    
    
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:message]];
    
}
- (IBAction)vatsimClicked:(id)sender {
    NSString * message = [NSString stringWithFormat:@"http://stats.vatsim.net/search_id.php?id=%@",_plane.cid];
    
    
    
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:message]];
}




@end
