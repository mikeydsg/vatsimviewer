//
//  VVPilotListViewController.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/11/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VVPilotListViewController : NSViewController <NSTableViewDelegate>

@property (strong) IBOutlet NSArrayController *arrayController;

@property NSMutableArray * store;

@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSBox *box;

-(void)updateData;


@end
