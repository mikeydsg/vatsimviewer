//
//  VVPilotListViewController.m
//  VatsimViewer
//
//  Created by Mike Evans on 2/11/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import "VVPilotListViewController.h"
#import "VVPlane.h"
#import "VVPilotDetail.h"



@interface VVPilotListViewController ()
@property VVPilotDetail * detail;

@end

@implementation VVPilotListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    return self;
}

-(void) awakeFromNib
{
    _arrayController.content=_store;
    [_tableView setTarget:self];
    
    [_tableView setDoubleAction:@selector(pilotDoubleClick:)];

   
    
}

- (void)pilotDoubleClick:(id)object {
    //NSLog(@"Controller Double Clicked!");
    
    unsigned long row = _tableView.selectedRow;
    VVPlane * plane = [_arrayController.arrangedObjects objectAtIndex:row];
    
    //NSLog(@"ATC: %@",atc.realname);
    
    _detail=[[VVPilotDetail alloc] initWithWindowNibName:@"VVPilotDetail"];
    _detail.plane=plane;
    
    [_detail showWindow:nil];   
    
    
}


-(BOOL) tableView:(NSTableView *)tableView shouldSelectTableColumn:(NSTableColumn *)tableColumn
{
    return NO;
    
}
-(void)updateData
{
    _arrayController.content=_store;

    
    [_tableView reloadData];
    NSString * count = [NSString stringWithFormat:@"%lu",_store.count];
    
    NSString * title = [[@"All Pilots (" stringByAppendingString:count] stringByAppendingString:@")"];
    
    _box.title=title;
    
    
}

@end
