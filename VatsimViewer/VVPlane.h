//
//  VVPlane.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/10/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVClient.h"


@interface VVPlane : VVClient

@property NSString * groundspeed;
@property NSString * aircraftType;
@property NSString * trueAirspeed;
@property NSString * departureAirport;
@property NSString * departureAirportDescription;
@property RSGeoPoint departureAirportLoc;
@property NSString * destinationAirport;
@property NSString * destinationAirportDescription;
@property RSGeoPoint destinationAirportLoc;
@property NSString * airspaceIn;
@property NSString * distanceFlown;
@property NSString * distanceToGo;
@property NSString * transponder;
@property NSString * plannedAltitude;
@property NSString * revision;
@property NSString * flightType;
@property NSString * plannedDepartureTime;
@property NSString * actualDepartureTime;
@property NSString * hoursEnroute;
@property NSString * minutesEnroute;
@property NSString * hoursFuel;
@property NSString * minutesFuel;
@property NSString * alternateAirport;
@property NSString * remarks;
@property NSString * route;
@property NSString * flightStatus;
@property NSString * eta;
@property NSColor * etaColor;

@property int heading;





/*
 QNH_iHg:39
 QNH_Mb:40
*/


@end
