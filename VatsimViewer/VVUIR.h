//
//  VVUIR.h
//  VatsimViewer
//
//  Created by Mike Evans on 2/16/14.
//  Copyright (c) 2014 Downstairs Geek LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VVUIR : NSObject
@property NSString * code;
@property NSString * name;
@property NSMutableArray * firs;

@end
